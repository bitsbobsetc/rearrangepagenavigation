﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace RearrangePageNavigation
{
    class MainPageViewModel
    {
        public MainPageViewModel()
        {
            Images = new ObservableCollection<ImageSource>();

            for (int i = 0; i < 10; i++)
            {
                var image = new BitmapImage(new Uri("ms-appx:///Assets/TestImage.jpg")) {DecodePixelHeight = 300};
                Images.Add(image);
            }
        }

        public ObservableCollection<ImageSource> Images { get; private set; } 
    }
}
